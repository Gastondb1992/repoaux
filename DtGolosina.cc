/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DtGolosina.cc
 * Author: gaston
 * 
 * Created on 19 de marzo de 2016, 19:20
 */

#include "DtGolosina.hh"

DtGolosina::DtGolosina() {
    this->nombre="";
    this->precio=0;
    this->fecha_venc=NULL;
    this->nombre_fabricante="";
    this->origen="";
}

DtGolosina::DtGolosina(const DtGolosina& orig) {
    this->nombre=orig.getNombre();
    this->precio=orig.getPrecio();
    this->fecha_venc=orig.setFechaVenc();
    this->nombre_fabricante=orig.getNombreFabricante();
    this->origen=orig.getOrigen();
}

DtGolosina::DtGolosina(string n, double p, Date fv, string nf, string o) {
    this->nombre=n;
    this->precio=p;
    this->fecha_venc=fv;
    this->nombre_fabricante=nf;
    this->origen=o;
}


DtGolosina::~DtGolosina() {
}
    
string DtGolosina::getNombre(){
    return this->nombre;
}
    
double DtGolosina::getPrecio(){
    return this->precio;
}
    
Date DtGolosina::getFechaVenc(){
    return this->fecha_venc;
}
    
string DtGolosina::getNombreFabricante(){
    return this->nombre_fabricante;
}
    
string DtGolosina::getOrigen(){
    return this->origen;
}
    
void DtGolosina::setNombre(string n){
    this->nombre=n;
}
    
void DtGolosina::setPrecio(double p){
    this->precio=p;
}
    
void DtGolosina::setFechaVenc(Date fv){
    this->fecha_venc=fv;
}
    
void DtGolosina::setNombreFabricante(string nf){
    this->nombre_fabricante=nf;
}
    
void DtGolosina::setOrigen(string o){
    this->origen=o;
}

