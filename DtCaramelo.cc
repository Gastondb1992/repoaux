/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DtCaramelo.cc
 * Author: gaston
 * 
 * Created on 19 de marzo de 2016, 19:21
 */

#include "DtCaramelo.hh"

DtCaramelo::DtCaramelo() {
    this->sabor="";
}

DtCaramelo::DtCaramelo(const DtCaramelo& orig) {
    this->sabor=orig.getSabor();
}

DtCaramelo::DtCaramelo(string n, double p, Date fv, string nf, string o, string s){
    this->sabor=s;
}

DtCaramelo::~DtCaramelo() {
}

void DtCaramelo::setSabor(string s){
    this->sabor= s;
}

string DtCaramelo::getSabor(){
    return this->sabor;
}
