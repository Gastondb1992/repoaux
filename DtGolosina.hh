/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DtGolosina.hh
 * Author: gaston
 *
 * Created on 19 de marzo de 2016, 19:20
 */
#ifndef DTGOLOSINA_HH
#define DTGOLOSINA_HH

#include "Date.hh"
#include <string>
using std::string;

class DtGolosina {
private:
    string nombre;
    double precio;
    Date fecha_venc;
    string nombre_fabricante;
    string origen;
public:
    DtGolosina();
    DtGolosina(string nombre, double precio, Date fecha_venc, string nombre_fabricante, string origen);
    DtGolosina(const DtGolosina& orig);
    virtual ~DtGolosina();
    string getNombre();
    double getPrecio();
    Date getFechaVenc();
    string getNombreFabricante();
    string getOrigen();
    void setNombre(string nombre);
    void setPrecio(double precio);
    void setFechaVenc(Date fecha_venc);
    void setNombreFabricante(string nombre_fabricante);
    void setOrigen(string origen);
};

#endif /* DTGOLOSINA_HH */

