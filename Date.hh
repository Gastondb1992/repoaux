/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Date.hh
 * Author: gaston
 *
 * Created on 19 de marzo de 2016, 19:18
 */

#ifndef DATE_HH
#define DATE_HH

class Date {
private:
    int dd;
    int mm;
    int aaaa;
public:
    Date();
    Date(int dd, int mm, int aaaa);
    Date(const Date& orig);
    virtual ~Date();
    int getDd();
    int getMm();
    int getAaaa();
    void setDd(int dd);
    void setMm(int mm);
    void setAaaa(int aaaa);
};

#endif /* DATE_HH */

