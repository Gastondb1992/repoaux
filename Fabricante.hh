/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Fabricante.hh
 * Author: gaston
 *
 * Created on 19 de marzo de 2016, 19:22
 */
#include "EnumOrigen.hh"
#include "Golosina.hh"
#include <string>
using std::string;

#ifndef FABRICANTE_HH
#define FABRICANTE_HH

class Fabricante {
private:
    string nombre;
    string rut;
    EnumOrigen origen;
    string pais;
    Golosina * golosinas_fabricadas;
public:
    Fabricante();
    Fabricante(string nombre, string rut, EnumOrigen origen, string pais, Golosina * golosinas_fabricadas);
    Fabricante(const Fabricante& orig);
    virtual ~Fabricante();
    string getNombre();
    string getRut();
    EnumOrigen getOrigen();
    string getPais();
    Golosina * getGolosinasFabricadas();
    void setNombre(string nombre);
    void setRut(string rut);
    void setOrigen(EnumOrigen origen);
    void setPais(string pais);
    void setGolosinasFabricadas(Golosina * golosinas_fabricadas);
};

#endif /* FABRICANTE_HH */

