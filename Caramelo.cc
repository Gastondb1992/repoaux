/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Caramelo.cc
 * Author: gaston
 * 
 * Created on 19 de marzo de 2016, 19:16
 */

#include "Caramelo.hh"

Caramelo::Caramelo() {
    this->sabor="";
}

Caramelo::Caramelo(const Caramelo& orig) {
    this->sabor=orig.getSabor();
}

Caramelo::Caramelo(string n, double p, Date fv, string s){
    this->sabor=s;
}

Caramelo::~Caramelo() {
}

void Caramelo::setSabor(string s){
    this->sabor= s;
}

string Caramelo::getSabor(){
    return this->sabor;
}
