/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DtAlfajor.hh
 * Author: gaston
 *
 * Created on 19 de marzo de 2016, 19:19
 */


#ifndef DTALFAJOR_HH
#define DTALFAJOR_HH

#include "DtGolosina.hh"
#include "Date.hh"
#include <string>
using std::string;

class DtAlfajor: public DtGolosina {
public:
    DtAlfajor();
    DtAlfajor(const DtAlfajor& orig);
    DtAlfajor(string nombre, double precio, Date fecha_venc, string nombre_fabricante, string origen);
    virtual ~DtAlfajor();

};

#endif /* DTALFAJOR_HH */

