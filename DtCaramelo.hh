/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   DtCaramelo.hh
 * Author: gaston
 *
 * Created on 19 de marzo de 2016, 19:21
 */

#ifndef DTCARAMELO_HH
#define DTCARAMELO_HH

#include "DtGolosina.hh"
#include <string>
using std::string;

class DtCaramelo: public DtGolosina {
private:
    string sabor;   
public:
    DtCaramelo();
    DtCaramelo(string nombre, double precio, Date fecha_venc, string nombre_fabricante, string origen, string sabor);
    DtCaramelo(const DtCaramelo& orig);
    virtual ~DtCaramelo();
    string getSabor();
    void setSabor(string sabor);
};

#endif /* DTCARAMELO_HH */

