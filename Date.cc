/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Date.cc
 * Author: gaston
 * 
 * Created on 19 de marzo de 2016, 19:18
 */

#include "Date.hh"

Date::Date() {
    this->aaaa=0;
    this->mm=0;
    this->dd=0;
}

Date::Date(const Date& orig) {
    this->aaaa=orig.getAaaa();
    this->mm=orig.getMm();
    this->dd=orig.getDd();
}

Date::Date(int d, int m, int a){
    this->aaaa=a;
    this->mm=m;
    this->dd=d;
}

Date::~Date() {
}

int Date::getAaaa(){
    return this->aaaa;
}

int Date::getMm(){
    return this->mm;
}

int Date::getDd(){
    return this->dd;
}

void Date::setAaaa(int a){
    this->aaaa=a;
}

void Date::setMm(int m){
    this->mm=m;
}

void Date::setDd(int d){
    this->dd=d;
}