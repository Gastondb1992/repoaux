/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Golosina.cc
 * Author: gaston
 * 
 * Created on 19 de marzo de 2016, 19:12
 */

#include "Golosina.hh"
#include <iostream>
#include <string>

Golosina::Golosina() {
    this->nombre="";
    this->precio=0;
    this->fecha_venc=NULL;
}

Golosina::Golosina(const Golosina& orig) {
    this->nombre=orig.getNombre();
    this->precio=orig.getPrecio();
    this->fecha_venc=orig.getFechaVenc();
}

Golosina::~Golosina() {
}

Golosina::Golosina(string n, double p, Date fv){
    this->nombre=n;
    this->precio=p;
    this->fecha_venc=fv;
}

string Golosina::getNombre(){
    return this->nombre;
}
    
double Golosina::getPrecio(){
    return this->precio;
}
    
Date Golosina::getFechaVenc(){
    return this->fecha_venc;
}
    
void Golosina::setNombre(string n){
    this->nombre=n;
}
    
void Golosina::setPrecio(double p){
    this->precio=p;
}
    
void Golosina::setFechaVenc(Date fv){
    this->fecha_venc=fv;
}
