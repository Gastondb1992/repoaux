/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Alfajor.hh
 * Author: gaston
 *
 * Created on 19 de marzo de 2016, 19:14
 */
#ifndef ALFAJOR_HH
#define ALFAJOR_HH

#include "Date.hh"
#include "Golosina.hh"
#include <string>
using std::string;

class Alfajor: public Golosina {
public:
    Alfajor();
    Alfajor(const Alfajor& orig);
    Alfajor(string nombre, double precio, Date fecha_venc);
    virtual ~Alfajor();

};

#endif /* ALFAJOR_HH */

