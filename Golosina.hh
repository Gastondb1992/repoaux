/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Golosina.hh
 * Author: gaston
 *
 * Created on 19 de marzo de 2016, 19:12
 */
#include "Date.hh"
#include <string>
using std::string;

#ifndef GOLOSINA_HH
#define GOLOSINA_HH

class Golosina {
private:
    string nombre;
    double precio;
    Date fecha_venc;
public: 
    Golosina();
    Golosina(const Golosina& orig);
    Golosina(string nombre, double precio, Date fecha_venc);
    virtual ~Golosina();
    string getNombre();
    double getPrecio();
    Date getFechaVenc();
    void setNombre(string nombre);
    void setPrecio(double precio);
    void setFechaVenc(Date fecha_venc);

};

#endif /* GOLOSINA_HH */

