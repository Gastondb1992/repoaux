/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Fabricante.cc
 * Author: gaston
 * 
 * Created on 19 de marzo de 2016, 19:22
 */

#include "Fabricante.hh"

Fabricante::Fabricante() {
    this->nombre="";
    this->rut="";
    this->origen="";
    this->pais="";
    this->golosinas_fabricadas=NULL;
}

Fabricante::Fabricante(const Fabricante& orig) {
    this->nombre=orig.getNombre();
    this->rut=orig.getRut();
    this->origen=orig.getOrigen();
    this->pais=orig.getPais();
    this->golosinas_fabricadas=orig.getGolosinasFabricadas();
}

Fabricante::Fabricante(string n, string r, EnumOrigen o, string p, Golosina* gf){
    this->nombre=n;
    this->rut=r;
    this->origen=o;
    this->pais=p;
    this->golosinas_fabricadas=gf;
}

Fabricante::~Fabricante() {
}

string Fabricante::getNombre(){
    return this->nombre;
}

string Fabricante::getRut(){
    return this->rut;
}

EnumOrigen Fabricante::getOrigen(){
    return this->origen;
}

string Fabricante::getPais(){
    return this->pais;
}

Golosina * Fabricante::getGolosinasFabricadas(){
    return this->golosinas_fabricadas;
}
    
void Fabricante::setNombre(string n){
    this->nombre=n;
}
    
void Fabricante::setRut(string r){
    this->rut=r;
}
    
void Fabricante::setOrigen(EnumOrigen o){
    this->origen=o;
}
    
void Fabricante::setPais(string p){
    this->pais=p;
}
    
void Fabricante::setGolosinasFabricadas(Golosina * gf){
    this->golosinas_fabricadas=gf;
}
