/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Caramelo.hh
 * Author: gaston
 *
 * Created on 19 de marzo de 2016, 19:16
 */
#ifndef CARAMELO_HH
#define CARAMELO_HH

#include "Golosina.hh"
#include <string>
using std::string;

class Caramelo: public Golosina {
private:
    string sabor;
public:
    Caramelo();
    Caramelo(string nombre, double precio, Date fecha_venc, string sabor);
    Caramelo(const Caramelo& orig);
    virtual ~Caramelo();
    string getSabor();
    void setSabor(string sabor);

};

#endif /* CARAMELO_HH */

